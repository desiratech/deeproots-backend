<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Services\PhotoGalleryService;

$pages = [
    '/',
    '/home',
    '/bio',
    '/learn',
    '/schedule',
    '/gal',
    '/book'
];

foreach ($pages as $page) {
    Route::get($page, 'HomeController@index')->name(str_replace('/', '', $page));
}

Route::get('/registration', function() {
    return File::get(public_path() . '/registration.html');
});

Route::get('/classes', function() {
    return File::get(public_path() . '/registration.html');
});

Auth::routes();
Route::get('/admin/logout', function() {
    if(Auth::check()) {
        Auth::logout();
    } 
   return redirect('/');
});

Route::middleware(['auth'])->group(function () {
    Route::get('/admin', 'AdminController@index')->name('home');
});