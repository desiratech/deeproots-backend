<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/mailing-list', ['middleware' => 'throttle:3,10', 'uses' => 'MailingListAddition@sendMail', 'as' => 'send-contact-form-email']);
Route::get('/photo-gal-images', ['middleware' => 'throttle:60,10', 'uses' => 'PhotoGallery@getImages', 'as' => 'get-images-for-photo-gal']);

Route::group(['prefix' => '/admin', 'middleware' => 'throttle:60,10'], function(){
    Route::post('/shows/create', ['uses' => 'ShowController@create', 'as' => 'create-show']);
    Route::get('/shows', ['uses' => 'ShowController@getShows', 'as' => 'get-all-shows']);
    Route::post('/shows/{id}/update', ['uses' => 'ShowController@update', 'as' => 'update-show']);
    Route::delete('/shows/{id}/delete', ['uses' => 'ShowController@delete', 'as' => 'delete-show']);
});


