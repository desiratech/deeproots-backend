<?php

namespace App\Models;

class Show 
{
    /** @var string */
    protected $name;

    /** @var string */
    protected $links;
    
    /** @var string */
    protected $description;

    /** @var int */
    protected $show_id;

    /** @var string */
    protected $date;

    public function __construct(
        string $showName,
        string $facebook,
        string $eventbrite,
        string $eventDescription,
        int $show_id,
        string $eventDate
        )
    {
        $this->name = $showName;
        $this->links = json_encode(['facebook' => $facebook, 'eventbrite' => $eventbrite], true);
        $this->description = $eventDescription;
        $this->show_id = $show_id;
        $this->date = $eventDate;
    }

    public function toArray() {
        return [
            'name' => $this->name,
            'links' => $this->links,
            'description' => $this->description,
            'show_id' => $this->show_id,
            'date' => $this->date
        ];
    }

    public function toFrontendResponseArray() {
        $links = json_decode($this->links);
        return [
             'showName' => $this->name,
             'facebook' => $links->facebook,
             'eventbrite' => $links->eventbrite,
             'eventDescription' => $this->description,
             'show_id' => $this->show_id,
             'eventDate' => $this->date
        ];
    }
}
