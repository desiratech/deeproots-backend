<?php

namespace App\Services;

class PhotoGalleryService {
    public function getImages() {
        $dir = 'img/gallery/';
        $imageFiles = scandir($dir);
        $images = [];
        foreach ($imageFiles as $key => $link) {
            if(is_dir($dir.$link) || $link === '..' || $link === '.'){
                continue;
            }

            $images[] = [
                'src' => $dir.$link,
                'alt' => '',
                'description' => $this->determineIfDescription($dir.$link),
                'category' => ''
            ];

        }
        return  json_encode($images);
    }

    private function determineIfDescription($imgPath) {
        if(strpos($imgPath, '*')) {
            return str_replace(['img/gallery/*', '.jpg'], '', $imgPath);
        } else {
            return null;
        }
    }
}