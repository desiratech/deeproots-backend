<?php

namespace App\Services;

use App\Models\Eloquent\User;

class UserService
{

    public function createUser(string $firstName, string $lastName, string $email, string $password)
    {
        $userEloquentModel = app()->make(User::class);

        $userEloquentModel->create([
            'name' => $firstName . ' ' . $lastName,
            'email' => $email,
            'password' => bcrypt($password),
        ]);

    }
}
