<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\UserService;

class createAdminUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:createAdmin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $first_name = $this->ask('What is the first name?');
        $last_name = $this->ask('What is the last name?');
        $email = $this->ask('What is the email address?');
        $password = $this->secret('What is the password?');

        /** UserService $userService */
       $userService = app()->make(UserService::class);

       try {
        $userService->createUser($first_name, $last_name, $email,$password);
        $this->info("User $first_name $last_name was created");
       } catch (Throwable $e) {
        $this->error("User $first_name $last_name was NOT created");
       }

    }
}
