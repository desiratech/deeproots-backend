<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PhotoGalleryService;

class HomeController extends Controller
{

    /** @var PhotoGalleryService */
    private $photoGalService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->photoGalService = new PhotoGalleryService();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pictures = $this->photoGalService->getImages();
        return view('index', ['pictures' => $pictures]);
    }
}
