<?php
namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Mail;
class MailingListAddition extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function sendMail(Request $request) {
        $from = $request->get('email');
        // $formattedMessage = $this->buildMessage($data);

        Mail::raw($from, function ($message) use ($from){
            $message->from($from, 'Mailing List Addition');
            $message->replyTo($from);
            $message->to(env('CONTACT_FORM_RECIPIENT'));
        });

        if (Mail::failures()) {
            return response()->json(['errors' => 'Something went wrong, we are looking into it. Try again soon.']);
        }

        return response()->json(['success' => 'Thank you for your message! Someone will contact you shortly.']);
    }

    // function buildMessage($data) {
    //     $from = $data['email'];
    //     $timestamp = $this->getTimestamp();
    //     $header = "New message from: " . $from ."\n" . "Timestamp: ". $timestamp;
    //     return $header . "\n\nMessage Body:\n" . $data['messageBody'];
    // }

    private function getTimestamp() {
        $date = Carbon::now('America/Winnipeg');
        return "{$date->format('l jS \\of F Y h:i:s A')}";
    }
}