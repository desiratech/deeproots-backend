<?php
namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;

class PhotoGallery extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function getImages(Request $request) {
        $dir = 'img/gallery/';
        $imageFiles = scandir($dir);
        $images = [];
        foreach ($imageFiles as $key => $link) {
            if(is_dir($dir.$link) || $link === '..' || $link === '.'){
                continue;
            }

            $images[] = [
                'src' => $dir.$link,
                'alt' => '',
                'description' => $this->determineIfDescription($dir.$link),
                'category' => ''
            ];

        }
        return response()->json(['images' => $images]);
    }

    private function determineIfDescription($imgPath) {
        if(strpos($imgPath, '*')) {
            return str_replace(['img/gallery/*', '.jpg'], '', $imgPath);
        } else {
            return null;
        }
    }
}