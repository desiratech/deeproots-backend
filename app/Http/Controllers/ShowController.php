<?php
namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use App\Models\Show;

class ShowController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
   
    public function create(Request $request)
    {
        \Log::info('create');
        $show = $this->createShowObject($request->all());

        
        \Log::info($show->toArray());
        \Log::info($show->toFrontendResponseArray());

        return response()->json($show->toFrontendResponseArray());
    }

    public function update(Request $request)
    {
        \Log::info('update');
        $show =  $this->createShowObject($request->all());
        \Log::info($show->toArray());
        \Log::info($show->toFrontendResponseArray());
    }

    public function delete(Request $request)
    {
        \Log::info('delete');
        \Log::info($request->all());
    }

    public function getShows(Request $request)
    {
        \Log::info('gothere');
    }

    private function createShowObject(array $requestData) {
        return new Show(
            $requestData['showName'], 
            $requestData['facebook'],
            $requestData['eventbrite'],
            $requestData['eventDescription'],
            $requestData['show_id'] ?? 0,
            $requestData['eventDate']
        );
    }
}