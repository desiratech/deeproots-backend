<html>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width" />
  <link rel="shortcut icon" href="http://www.deeproots-strongdance.com/favic0n.png"/>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="keywords" content="Winnipeg bellydance, Bellydance classes, Belly Dance, Fire Bellydance, Bellydance Workshops, dance manitoba, tribal style, community">
  <meta name="description" content="Bellydance classes & upcoming performances in Winnipeg. Available for hire and private/class instruction and performances.">
  <title>Deep Roots - Strong Dance: Winnipeg Bellydance Performances &amp; Classes</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
  <link rel="stylesheet" href="./css/bootswatch.css" />
  <link rel="stylesheet" href="./css/app.css" />
  <link rel="stylesheet" href="owl-carousel/assets/owl.carousel.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <link rel="stylesheet" href="owl-carousel/assets/owl.theme.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
    crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Philosopher" rel="stylesheet">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
    crossorigin="anonymous"></script>
  <!-- jsDeliver -->
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.6/jquery.lazy.min.js"></script>
  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.6/jquery.lazy.plugins.min.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-36314115-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-36314115-1');
</script>
</head>

<body>
  <script>
      window.pictures = {!!$pictures!!};
  </script> 
  <div id="app"> </div>
  <script type="text/javascript" src="owl-carousel/owl.carousel.js"></script>
  <script src="./js/app.js"></script>
  <script>
    $(document).ready(function () {
      $('.lazyload').Lazy({
        // your configuration goes here
        scrollDirection: 'vertical',
        effect: 'fadeIn',
        effectTime: 1000,
        visibleOnly: true,
        onError: function (element) {
          console.log('error loading ' + element.data('src'));
        }
      });
      // Add smooth scrolling to all links
      $("a").on('click', function (event) {
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();

          // Store hash
          var hash = this.hash;

          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function () {

            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if
      });
    });
  </script>
</body>

</html>